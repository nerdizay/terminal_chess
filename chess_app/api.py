from ninja import NinjaAPI, Schema
from chess_app.chess_core import *
from memory_profiler import profile



api = NinjaAPI()

class Key(Schema):
    name: str
    
games = []


@api.post("/game/new")
@profile
def create_game(request):
    game = Game(
        create_classic_deck(),
        ChooseFigure(ChooseFigureKeyboardHandlerTerminal()),
        Terminal(),
    )
    games.append(game)
    game_id = len(games) - 1
    game = games[game_id]
    return {"game_id": game_id, "deck_info":  game.display(game.default_message)}


@api.post("/game/{game_id}/press_key")
def press_key(request, game_id: int, key: Key):
    try:
        return {"game_id": game_id, "deck_info": games[game_id].handle_keyboard_event(key)}
    except IndexError:
        return {"error": "not found"}