# from chess_app.chess_core.display import Terminal
# from chess_app.chess_core.deck import Deck, create_classic_deck
# from chess_app.chess_core.states import Greeting
# from chess_app.chess_core.keyboard_handler import GreetingKeyboardHandlerTerminal
# from chess_app.chess_core.main import Game
from .display import Terminal
from .deck import Deck, create_classic_deck
from .states import Greeting, ChooseFigure
from .keyboard_handler import GreetingKeyboardHandlerTerminal, ChooseFigureKeyboardHandlerTerminal
from .main import Game

