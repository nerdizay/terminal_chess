from enum import Enum


class FigureVariant(Enum):
    rook = "rook" # Ладья
    knight = "knight" # Конь
    bishop = "bishop" # Слон
    king = "king" # Король
    queen = "queen" # Ферзь
    pawn = "pawn"
    
    
class Color(Enum):
    black = "black"
    white = "white"


class CellName(Enum):
    a8 = "a8"
    b8 = "b8"
    c8 = "c8"
    d8 = "d8"
    e8 = "e8"
    f8 = "f8"
    g8 = "g8"
    h8 = "h8"
    
    a7 = "a7"
    b7 = "b7"
    c7 = "c7"
    d7 = "d7"
    e7 = "e7"
    f7 = "f7"
    g7 = "g7"
    h7 = "h7"
    
    a6 = "a6"
    b6 = "b6"
    c6 = "c6"
    d6 = "d6"
    e6 = "e6"
    f6 = "f6"
    g6 = "g6"
    h6 = "h6"
    
    a5 = "a5"
    b5 = "b5"
    c5 = "c5"
    d5 = "d5"
    e5 = "e5"
    f5 = "f5"
    g5 = "g5"
    h5 = "h5"
    
    a4 = "a4"
    b4 = "b4"
    c4 = "c4"
    d4 = "d4"
    e4 = "e4"
    f4 = "f4"
    g4 = "g4"
    h4 = "h4"
    
    a3 = "a3"
    b3 = "b3"
    c3 = "c3"
    d3 = "d3"
    e3 = "e3"
    f3 = "f3"
    g3 = "g3"
    h3 = "h3"
    
    a2 = "a2"
    b2 = "b2"
    c2 = "c2"
    d2 = "d2"
    e2 = "e2"
    f2 = "f2"
    g2 = "g2"
    h2 = "h2"
    
    a1 = "a1"
    b1 = "b1"
    c1 = "c1"
    d1 = "d1"
    e1 = "e1"
    f1 = "f1"
    g1 = "g1"
    h1 = "h1"


SIMBOLS_DECK = ["a", "b", "c", "d", "e", "f", "g", "h"]
NUMS_DECK = ["8", "7", "6", "5", "4", "3", "2", "1"]    


class BorderDeck(Enum):
    top_border = 56
    bottom_border = 49
    left_border = 97
    right_border = 104
    

class MovementVariant(Enum):
    up = "up"
    down = "down"
    left = "left"
    right = "right"
    up_left = "up_left"
    up_right = "up_right"
    down_left = "down_left"
    down_right = "down_right"
    L_up_up_left = "L_up_up_left"
    L_up_up_right = "L_up_up_right"
    L_down_down_left = "L_down_down_left"
    L_down_down_right = "L_down_down_right"
    L_up_left_left = "L_up_left_left"
    L_up_right_right = "L_up_right_right"
    L_down_left_left = "L_down_left_left"
    L_down_right_right = "L_down_right_right"
    

MAX_SIZE_STEP = {
    FigureVariant.rook.value: 8,
    FigureVariant.knight.value: 1,
    FigureVariant.bishop.value: 8,
    FigureVariant.king.value: 1,
    FigureVariant.queen.value: 8,
    FigureVariant.pawn.value: 2,
}

DIRECTION_POSSIBLE_MOVEMENT = {
    Color.black.value: {
        FigureVariant.rook.value: [
            MovementVariant.up.value,
            MovementVariant.down.value,
            MovementVariant.left.value,
            MovementVariant.right.value,
        ],
        FigureVariant.knight.value: [
            MovementVariant.L_up_up_left.value,
            MovementVariant.L_up_up_right.value,
            MovementVariant.L_down_down_left.value,
            MovementVariant.L_down_down_right.value,
            MovementVariant.L_up_left_left.value,
            MovementVariant.L_up_right_right.value,
            MovementVariant.L_down_left_left.value,
            MovementVariant.L_down_right_right.value,
        ],
        FigureVariant.bishop.value: [
            MovementVariant.up_left.value,
            MovementVariant.up_right.value,
            MovementVariant.down_left.value,
            MovementVariant.down_right.value,
        ],
        FigureVariant.king.value: [
            MovementVariant.up.value,
            MovementVariant.down.value,
            MovementVariant.left.value,
            MovementVariant.right.value,
            MovementVariant.up_left.value,
            MovementVariant.up_right.value,
            MovementVariant.down_left.value,
            MovementVariant.down_right.value,
        ],
        FigureVariant.queen.value: [
            MovementVariant.up.value,
            MovementVariant.down.value,
            MovementVariant.left.value,
            MovementVariant.right.value,
            MovementVariant.up_left.value,
            MovementVariant.up_right.value,
            MovementVariant.down_left.value,
            MovementVariant.down_right.value,
        ],
        FigureVariant.pawn.value: [
            MovementVariant.down.value,
            MovementVariant.down_left.value,
            MovementVariant.down_right.value,
        ]
    },
    Color.white.value: {
        FigureVariant.rook.value: [
            MovementVariant.up.value,
            MovementVariant.down.value,
            MovementVariant.left.value,
            MovementVariant.right.value,
        ],
        FigureVariant.knight.value: [
            MovementVariant.L_up_up_left.value,
            MovementVariant.L_up_up_right.value,
            MovementVariant.L_down_down_left.value,
            MovementVariant.L_down_down_right.value,
            MovementVariant.L_up_left_left.value,
            MovementVariant.L_up_right_right.value,
            MovementVariant.L_down_left_left.value,
            MovementVariant.L_down_right_right.value,
        ],
        FigureVariant.bishop.value: [
            MovementVariant.up_left.value,
            MovementVariant.up_right.value,
            MovementVariant.down_left.value,
            MovementVariant.down_right.value,
        ],
        FigureVariant.king.value: [
            MovementVariant.up.value,
            MovementVariant.down.value,
            MovementVariant.left.value,
            MovementVariant.right.value,
            MovementVariant.up_left.value,
            MovementVariant.up_right.value,
            MovementVariant.down_left.value,
            MovementVariant.down_right.value,
        ],
        FigureVariant.queen.value: [
            MovementVariant.up.value,
            MovementVariant.down.value,
            MovementVariant.left.value,
            MovementVariant.right.value,
            MovementVariant.up_left.value,
            MovementVariant.up_right.value,
            MovementVariant.down_left.value,
            MovementVariant.down_right.value,
        ],
        FigureVariant.pawn.value: [
            MovementVariant.up.value,
            MovementVariant.up_left.value,
            MovementVariant.up_right.value,
        ]
    }
}