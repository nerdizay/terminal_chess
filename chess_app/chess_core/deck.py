from .constants import CellName, FigureVariant, Color, NUMS_DECK, SIMBOLS_DECK
from .figure import Figure
from typing import List


class Cell:
    def __init__(self, name: CellName, figure: Figure = None):
        self.name = name
        if figure is not None:
            self.figure = figure
            self.figure.cell = self
        else:
            self.figure = None
            
    def __str__(self):
        return f"{self.name}"
        

C = Cell
F = Figure
FV = FigureVariant


class Deck:
    def __init__(self, deck: List[Cell]):
        self.deck = deck
        self.current_cell = None
        self.focus_cell = "a1"
        for line in deck:
            for cell in line:
                cell.deck = self

    def __getitem__(self, name: CellName) -> Cell:
        simbol, num = [*name]
        if num in NUMS_DECK and simbol in SIMBOLS_DECK:
            return self.deck[NUMS_DECK.index(num)][SIMBOLS_DECK.index(simbol)]
        else:
            raise Exception

    @property
    def focus_cell(self) -> Cell:
        return self._focus_cell
    
    @focus_cell.setter
    def focus_cell(self, cell: Cell | CellName):
        if isinstance(cell, Cell):
            self._focus_cell = cell
        elif isinstance(cell, str):
            self._focus_cell = self[cell]
            
    @property
    def current_cell(self) -> Cell:
        return self._current_cell
    
    @current_cell.setter
    def current_cell(self, cell: Cell | CellName):
        if isinstance(cell, Cell):
            self._current_cell = cell
        elif isinstance(cell, str):
            self._current_cell = self[cell]
        elif cell is None:
            self._current_cell = None
            
    @property
    def possible_next_cells(self):
        if self._current_cell is not None:
            figure = self._current_cell.figure
            if figure is not None:
                return [self[cell] for cell in figure.get_possible_next_cells()]
        return []


def create_classic_deck() -> Deck:
    return Deck([
        [
            C("a8", F(FV.rook.value, Color.black.value)),
            C("b8", F(FV.knight.value, Color.black.value)),
            C("c8", F(FV.bishop.value, Color.black.value)),
            C("d8", F(FV.king.value, Color.black.value)),
            C("e8", F(FV.queen.value, Color.black.value)),
            C("f8", F(FV.bishop.value, Color.black.value)),
            C("g8", F(FV.knight.value, Color.black.value)),
            C("h8", F(FV.rook.value, Color.black.value)),
        ],
        [
            C("a7", F(FV.pawn.value, Color.black.value)),
            C("b7", F(FV.pawn.value, Color.black.value)),
            C("c7", F(FV.pawn.value, Color.black.value)),
            C("d7", F(FV.pawn.value, Color.black.value)),
            C("e7", F(FV.pawn.value, Color.black.value)),
            C("f7", F(FV.pawn.value, Color.black.value)),
            C("g7", F(FV.pawn.value, Color.black.value)),
            C("h7", F(FV.pawn.value, Color.black.value)),
        ],
        [C("a6"), C("b6"), C("c6"), C("d6"), C("e6"), C("f6"), C("g6"), C("h6")],
        [C("a5"), C("b5"), C("c5"), C("d5"), C("e5"), C("f5"), C("g5"), C("h5")],
        [C("a4"), C("b4"), C("c4"), C("d4"), C("e4"), C("f4"), C("g4"), C("h4")],
        [C("a3"), C("b3"), C("c3"), C("d3"), C("e3"), C("f3"), C("g3"), C("h3")],
        [
            C("a2", F(FV.pawn.value, Color.white.value)),
            C("b2", F(FV.pawn.value, Color.white.value)),
            C("c2", F(FV.pawn.value, Color.white.value)),
            C("d2", F(FV.pawn.value, Color.white.value)),
            C("e2", F(FV.pawn.value, Color.white.value)),
            C("f2", F(FV.pawn.value, Color.white.value)),
            C("g2", F(FV.pawn.value, Color.white.value)),
            C("h2", F(FV.pawn.value, Color.white.value)),
        ],
        [
            C("a1", F(FV.rook.value, Color.white.value)),
            C("b1", F(FV.knight.value, Color.white.value)),
            C("c1", F(FV.bishop.value, Color.white.value)),
            C("d1", F(FV.king.value, Color.white.value)),
            C("e1", F(FV.queen.value, Color.white.value)),
            C("f1", F(FV.bishop.value, Color.white.value)),
            C("g1", F(FV.knight.value, Color.white.value)),
            C("h1", F(FV.rook.value, Color.white.value)),
        ],
    ])
