import os
from abc import ABC, abstractmethod
from .states import Greeting
from django.template import Context, Template
from jinja2 import Environment, FileSystemLoader

env = Environment(loader=FileSystemLoader('.'))


class Displayer(ABC):
    
    @abstractmethod
    def display(self):
        raise NotImplemented

    
class Terminal(Displayer):
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    
        
    def green(self, value: str) -> str:
        colored = self.OKGREEN + value + self.ENDC
        return colored
    
    def blue(self, value: str) -> str:
        colored = self.OKCYAN + value + self.ENDC
        return colored
    
    def yellow(self, value: str) -> str:
        colored = self.WARNING + value + self.ENDC
        return colored
    
    def red(self, value: str) -> str:
        colored = self.FAIL + value + self.ENDC
        return colored
    
    def bold(self, value: str) -> str:
        colored = self.BOLD + value + self.ENDC
        return colored
    
    def clear(self) -> None:
        os.system('cls')

    def display(self, deck, state, message: dict | None = None) -> None:
        # if isinstance(state, Greeting):
        #     res = {
        #         "message": env.get_template('templates/greeting.html').render(),
        #         "deck_info": None
        #     }

        # else:
            
        res = {}
        res_deck = []
        for line in deck.deck:
            
            res_line = []
            for cell in line:
                
                res_cell = {}
                cell_figure = cell.figure
                
                if cell_figure is not None:
                    res_cell["figure"] = cell_figure.name
                    res_cell["color"] = cell_figure.color
                else:
                    res_cell["figure"] = None
                    res_cell["color"] = None
                    
                res_cell["name"] = cell.name
                
                res_line.append(res_cell)
                
            res_deck.append(res_line)
            res["deck"] = res_deck
        
        res["focus_cell"] = deck.focus_cell.name if deck.focus_cell else None
        res["current_cell"] = deck.current_cell.name if deck.current_cell else None
        res["possible_next_cells"] = [cell.name for cell in deck.possible_next_cells]
        
        if message:
            res["message"] = message
                 
        return res