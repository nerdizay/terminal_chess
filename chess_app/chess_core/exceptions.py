class OutOfBorder(Exception):
    pass

class UnknownKey(Exception):
    pass

class UnpossibleMovement(Exception):
    pass

class AnotherColorTeamShouldMove(Exception):
    pass