from .constants import (
    FigureVariant,
    Color,
    MAX_SIZE_STEP,
    DIRECTION_POSSIBLE_MOVEMENT,
    CellName,
    MovementVariant
)
from .exceptions import OutOfBorder, UnpossibleMovement
from .movement import Movement
from typing import List
import copy


class Figure:
    def __init__(self, name: FigureVariant, color: Color):
        self.name = name
        self.color = color
        self.direction_moving_possible = DIRECTION_POSSIBLE_MOVEMENT[self.color][self.name]
        self.max_size_step = MAX_SIZE_STEP[self.name]
        self.cell = None
        self.deck = None
        
    def __repr__(self):
        return f"{self.name}"
    
    def get_possible_next_cells(self) -> List[CellName]:
        movement = Movement()
        possible_next_cells = []
        current_color = self.color
        if self.name == FigureVariant.pawn.value:
            
            if self.cell.name[1] in ["2", "7"]:
                self.max_size_step = 2
            else:
                self.max_size_step = 1
                
            DIRECTION_FOR_KILL_ENEMY = [
                MovementVariant.up_left.value,
                MovementVariant.up_right.value,
                MovementVariant.down_left.value,
                MovementVariant.down_right.value,
            ]
                
            for direction in self.direction_moving_possible:
                for step in range(1, self.max_size_step+1):
                    try:
                        
                        potential_possible_next_cell = getattr(movement, direction)(self.cell.name, step)
                        figure_on_the_way = self.cell.deck[potential_possible_next_cell].figure
                        
                        if direction in DIRECTION_FOR_KILL_ENEMY:
                            if figure_on_the_way is None:
                                break
                            
                        if direction not in DIRECTION_FOR_KILL_ENEMY:
                            if figure_on_the_way is not None:
                                break
                        
                        if figure_on_the_way is not None:
                            if figure_on_the_way.color == current_color:
                                break
                            else:
                                possible_next_cells.append(potential_possible_next_cell)
                                break
                        possible_next_cells.append(potential_possible_next_cell)
                        
                    except OutOfBorder:
                        continue
        else:
            for direction in self.direction_moving_possible:
                for step in range(1, self.max_size_step+1):
                    try:
                        
                        potential_possible_next_cell = getattr(movement, direction)(self.cell.name, step)
                        figure_on_the_way = self.cell.deck[potential_possible_next_cell].figure
                        
                        if figure_on_the_way is not None:
                            if figure_on_the_way.color == current_color:
                                break
                            else:
                                possible_next_cells.append(potential_possible_next_cell)
                                break
                        possible_next_cells.append(potential_possible_next_cell)
                        
                    except OutOfBorder:
                        continue
        return possible_next_cells
    
    def move(self, cell):
        if cell.name not in self.get_possible_next_cells():
            raise UnpossibleMovement
        self.cell.figure = None
        figure = copy.deepcopy(self)
        figure.cell = cell
        cell.figure = figure

