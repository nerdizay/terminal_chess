from .exceptions import (
    UnknownKey,
    OutOfBorder,
    UnpossibleMovement,
    AnotherColorTeamShouldMove,
)
from abc import ABC, abstractmethod
from .movement import Movement
from .constants import Color


class KeyboardHandlerAbstract(ABC):
    
    @abstractmethod
    def get_key(self, event):
        raise NotImplemented
    
    def handle_key(self, key: str) -> None:
        handle = getattr(self, f"handle_{key.upper()}")
        handle()
    
    def handle_keyboard_event(self, event) -> None:
        key = self.get_key(event)
        self.handle_key(key)
    
    @abstractmethod
    def handle_W(self):
        raise NotImplemented
    
    @abstractmethod
    def handle_A(self):
        raise NotImplemented
    
    @abstractmethod
    def handle_S(self):
        raise NotImplemented
    
    @abstractmethod
    def handle_D(self):
        raise NotImplemented
    
    @abstractmethod
    def handle_ENTER(self):
        raise NotImplemented
    
    @abstractmethod
    def handle_ESC(self):
        raise NotImplemented


class KeyboardHandlerTerminalBase(KeyboardHandlerAbstract):
    __CONVERTER_RUS_TO_EN = {
        "в": "d", "ц": "w", "ф": "a", "ы": "s",
        "d": "d", "w": "w", "a": "a", "s": "s",
        "enter": "enter",
        "esc": "esc"
    }
    
    def get_key(self, event):
        key = self.__CONVERTER_RUS_TO_EN.get(event.name)
        if key is None:
            raise UnknownKey
        return key
    
    def handle_W(self):
        movement = Movement()
        focus_cell = self.state.context.deck.focus_cell
        try:
            self.state.context.deck.focus_cell = movement.up(focus_cell.name)
        except OutOfBorder:
            return
    
    def handle_A(self):
        movement = Movement()
        focus_cell = self.state.context.deck.focus_cell
        try:
            self.state.context.deck.focus_cell = movement.left(focus_cell.name)
        except OutOfBorder:
            return
    
    def handle_S(self):
        movement = Movement()
        focus_cell = self.state.context.deck.focus_cell
        try:
            self.state.context.deck.focus_cell = movement.down(focus_cell.name)
        except OutOfBorder:
            return
    
    def handle_D(self):
        movement = Movement()
        focus_cell = self.state.context.deck.focus_cell
        try:
            self.state.context.deck.focus_cell = movement.right(focus_cell.name)
        except OutOfBorder:
            return
    
    def handle_ENTER(self):
        raise NotImplemented
    
    def handle_ESC(self):
        raise NotImplemented
    
    
class GreetingKeyboardHandlerTerminal(KeyboardHandlerTerminalBase):
    
    def handle_W(self):
        return
    
    def handle_A(self):
        return
    
    def handle_S(self):
        return
    
    def handle_D(self):
        return
    
    def handle_ENTER(self):
        self.state.context.deck.current_cell = None
        self.state.context.set_state(self.state.next_state())
        
    def handle_ESC(self):
        return
    
class ChooseFigureKeyboardHandlerTerminal(KeyboardHandlerTerminalBase):
    
    def handle_ENTER(self):
        if self.state.context.deck.focus_cell.figure is None:
            return
        if self.state.context.whose_move != self.state.context.deck.focus_cell.figure.color:
            raise AnotherColorTeamShouldMove
        self.state.context.deck.current_cell = self.state.context.deck.focus_cell
        self.state.context.set_state(self.state.next_state())
            
    def handle_ESC(self):
        return
            
    
class MoveFigureKeyboardHandlerTerminal(KeyboardHandlerTerminalBase):
    
    def handle_ENTER(self):
        current_figure = self.state.context.deck.current_cell.figure
        focus_cell = self.state.context.deck.focus_cell
        current_figure.move(focus_cell)
        self.state.context.deck.current_cell = None
        self.state.context.set_state(self.state.next_state())
        current_color = self.state.context.whose_move
        self.state.context.whose_move = Color.white.value if current_color == Color.black.value else Color.black.value
        return
    
    def handle_ESC(self):
        self.state.context.deck.current_cell = None
        self.state.context.set_state(self.state.previous_state())
        return
    
class CheckToTheKingKeyboardHandlerTerminal(KeyboardHandlerTerminalBase):
    
    def handle_ENTER(self):
        return
    
    def handle_ESC(self):
        return
    
    
class CheckmateKeyboardHandlerTerminal(KeyboardHandlerTerminalBase):
    
    def handle_ENTER(self):
        return
    
    def handle_ESC(self):
        return

    
    
    
    
    
    
