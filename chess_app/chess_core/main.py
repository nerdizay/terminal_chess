import keyboard
import time
from .states import State, ChooseFigure, Greeting
from .deck import Cell, Deck, Figure
from .constants import FigureVariant, Color
from .display import Displayer
from .keyboard_handler import GreetingKeyboardHandlerTerminal
from .exceptions import UnpossibleMovement, AnotherColorTeamShouldMove


class Game:
    
    def __init__(
        self,
        deck: Deck,
        state: State,
        displayer: Displayer,
    ):
        self.deck = deck
        self.displayer = displayer
        self.set_state(state)
        self.whose_move = Color.white.value
        
    def display(self, message: dict | None = None):
        return self.displayer.display(self.deck, self.state, message)
    
    @property
    def default_message(self):
        return {
            "text": "Ход белых" if self.whose_move == Color.white.value else "Ход черных",
            "type": "info",
        }
        
    def handle_keyboard_event(self, event):
        try:
            self.state.handle_keyboard_event(event)
            
        except UnpossibleMovement:
            return self.display({
                "text": "Движение невозможно",
                "type": "error",
            })
        
        except AnotherColorTeamShouldMove:
            return self.display({
                "text": "Cейчас должен ходить другой",
                "type": "error",
            })
        
        return self.display(self.default_message)
        
    def set_state(self, state: State):
        self.state = state
        self.state.context = self


if __name__ == "__main__":

    from deck import create_classic_deck
    from display import Terminal
        
    displayer = Terminal()
    deck = create_classic_deck()
    state = Greeting(GreetingKeyboardHandlerTerminal())
    game = Game(deck, state, displayer)
    game.display()
    
    for key in ["w", "a", "s", "d", "enter", "esc"]:
        keyboard.on_press_key(key, game.handle_keyboard_event)
        
    while True:
        time.sleep(666)
