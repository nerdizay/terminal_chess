from .constants import CellName, BorderDeck
from .exceptions import OutOfBorder


class Movement:
    def __init__(self):
        pass
    
    def __validate(self, cell: CellName):
        vertical = ord(cell[1])
        horizont = ord(cell[0])
        if vertical > BorderDeck.top_border.value or vertical < BorderDeck.bottom_border.value:
            raise OutOfBorder
        if horizont < BorderDeck.left_border.value or horizont > BorderDeck.right_border.value:
            raise OutOfBorder
        
    def __up(self, cell: CellName) -> CellName:
        self.__validate(cell)
        cell = cell[0]+chr(ord(cell[1])+1)
        self.__validate(cell)
        return cell
    
    def __down(self, cell: CellName) -> CellName:
        self.__validate(cell)
        cell = cell[0]+chr(ord(cell[1])-1)
        self.__validate(cell)
        return cell
    
    def __left(self, cell: CellName) -> CellName:
        self.__validate(cell)
        cell = chr(ord(cell[0])-1)+cell[1]
        self.__validate(cell)
        return cell
    
    def __right(self, cell: CellName) -> CellName:
        self.__validate(cell)
        cell = chr(ord(cell[0])+1)+cell[1]
        self.__validate(cell)
        return cell
    
    def up(self, cell: CellName, size_step: int = 1) -> CellName:
        for _ in range(size_step):
            cell = self.__up(cell)
        return cell
    
    def down(self, cell: CellName, size_step: int = 1) -> CellName:
        for _ in range(size_step):
            cell = self.__down(cell)
        return cell
    
    def left(self, cell: CellName, size_step: int = 1) -> CellName:
        for _ in range(size_step):
            cell = self.__left(cell)
        return cell
    
    def right(self, cell: CellName, size_step: int = 1) -> CellName:
        for _ in range(size_step):
            cell = self.__right(cell)
        return cell
    
    def up_right(self, cell: CellName, size_step: int = 1) -> CellName:
        for _ in range(size_step):
            cell = self.__up(cell)
            cell = self.__right(cell)
        return cell
    
    def up_left(self, cell: CellName, size_step: int = 1) -> CellName:
        for _ in range(size_step):
            cell = self.__up(cell)
            cell = self.__left(cell)
        return cell
    
    def down_right(self, cell: CellName, size_step: int = 1) -> CellName:
        for _ in range(size_step):
            cell = self.__down(cell)
            cell = self.__right(cell)
        return cell
    
    def down_left(self, cell: CellName, size_step: int = 1) -> CellName:
        for _ in range(size_step):
            cell = self.__down(cell)
            cell = self.__left(cell)
        return cell
    
    def L_up_up_left(self, cell: CellName, *args, **kwargs) -> CellName:
        size_step = 1
        for _ in range(size_step):
            cell = self.__up(cell)
            cell = self.__up(cell)
            cell = self.__left(cell)
        return cell
    
    def L_up_up_right(self, cell: CellName, *args, **kwargs) -> CellName:
        size_step = 1
        for _ in range(size_step):
            cell = self.__up(cell)
            cell = self.__up(cell)
            cell = self.__right(cell)
        return cell
    
    def L_down_down_left(self, cell: CellName, *args, **kwargs) -> CellName:
        size_step = 1
        for _ in range(size_step):
            cell = self.__down(cell)
            cell = self.__down(cell)
            cell = self.__left(cell)
        return cell
    
    def L_down_down_right(self, cell: CellName, *args, **kwargs) -> CellName:
        size_step = 1
        for _ in range(size_step):
            cell = self.__down(cell)
            cell = self.__down(cell)
            cell = self.__right(cell)
        return cell
    
    def L_up_left_left(self, cell: CellName, *args, **kwargs) -> CellName:
        size_step = 1
        for _ in range(size_step):
            cell = self.__up(cell)
            cell = self.__left(cell)
            cell = self.__left(cell)
        return cell
    
    def L_up_right_right(self, cell: CellName, *args, **kwargs) -> CellName:
        size_step = 1
        for _ in range(size_step):
            cell = self.__up(cell)
            cell = self.__right(cell)
            cell = self.__right(cell)
        return cell
    
    def L_down_left_left(self, cell: CellName, *args, **kwargs) -> CellName:
        size_step = 1
        for _ in range(size_step):
            cell = self.__down(cell)
            cell = self.__left(cell)
            cell = self.__left(cell)
        return cell
    
    def L_down_right_right(self, cell: CellName, *args, **kwargs) -> CellName:
        size_step = 1
        for _ in range(size_step):
            cell = self.__down(cell)
            cell = self.__right(cell)
            cell = self.__right(cell)
        return cell
