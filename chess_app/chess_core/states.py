from abc import ABC, abstractmethod
from collections.abc import Callable, Awaitable
from .deck import Deck
from .keyboard_handler import (
    KeyboardHandlerAbstract,
    ChooseFigureKeyboardHandlerTerminal,
    MoveFigureKeyboardHandlerTerminal,
    CheckmateKeyboardHandlerTerminal,
)



class State(ABC):
    
    def __init__(self, keyboard_handler: KeyboardHandlerAbstract):
        self.keybord_handler = keyboard_handler
        self.keybord_handler.state = self
    
    @property
    def deck(self) -> Deck:
        return self.context.deck
    
    def handle_keyboard_event(self, event) -> None:
        self.keybord_handler.handle_keyboard_event(event)    
        

class ChooseFigure(State):
    
    def next_state(self):
        return MoveFigure(MoveFigureKeyboardHandlerTerminal())
    

class MoveFigure(State):
    
    def next_state(self):
        return ChooseFigure(ChooseFigureKeyboardHandlerTerminal())
        # return Checkmate(CheckmateKeyboardHandlerTerminal())
    
    def previous_state(self):
        return ChooseFigure(ChooseFigureKeyboardHandlerTerminal())


class Greeting(State):
    
    def next_state(self):
        return ChooseFigure(ChooseFigureKeyboardHandlerTerminal())
    
class CheckToTheKing(State):
    pass
    
class Checkmate(State):
    pass

