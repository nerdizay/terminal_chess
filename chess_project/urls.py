from django.contrib import admin
from django.urls import path
from chess_app.api import api
from django.shortcuts import render


def index(request):
    return render(request, "index.html")


urlpatterns = [
    # path('admin/', admin.site.urls),
    path("", index),
    path("api/", api.urls),
]
