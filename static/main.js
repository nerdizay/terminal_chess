const createGameBtn = document.querySelector('#createGameBtn')

function customFetch(endpoint, method, body, callback){
    // endpoint should begin with slash
    if (body){
        request = {
            method,
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(body)
        }
    }else{
        request = {
            method,
            headers: {
                'Content-Type': 'application/json',
            },
        }
    }
    fetch(location.protocol+'//'+location.host+endpoint(), request).then((response)=>{
        if (response.status != 200 & response.status != 201) {
            throw new Error("Ошибка!");
        }
        return response.json()
    }).then((response)=>{
        if (response.error){
            throw new Error(response.error)
        }
        callback(response)
    }).catch(err => {
        console.error(err)
    })
}

class Endpoint{
    constructor(pattern, prop){
        this.pattern = pattern
        this.prop = prop
    }
    toString(){
        return this.pattern.replace("%prop%", this.prop)
    }
}

class State{
    constructor(runningGame, gameID){
        this.runningGame = runningGame
        this._gameID = gameID
    }
    get gameID(){
        return this._gameID
    }
    set gameID(value){
        return this._gameID = value
    }
}

const state = new State(null, null)

const api = {
    createGame: {
        method: "POST",
        endpoint(){
            return "/api/game/new"
        } 
    },
    pressKey: {
        method: "POST",
        endpoint(){
            return new Endpoint("/api/game/%prop%/press_key", state.gameID) 
        }
    }
    
}


class Figure{
    constructor(name, color){
        this.name = name
        this.color = color
    }
    getHTMLStructure(){
        if (this.name && this.color){
            return `<img class="figure" 
                        draggable="false" 
                        style="cursor: pointer;" 
                        src="/static/figures/${this.name}_${this.color}.svg"  
                        width="60px" height="60px">
                    </img>`
        } else { 
            return "" 
        }
    }
}

class Deck{
    // deck - массив Сell
    constructor(deck, focus_cell, current_cell, possible_next_cells) {
        this.refreshData(deck, focus_cell, current_cell, possible_next_cells)
    }
    refreshData(deck, focus_cell, current_cell, possible_next_cells){
        this.deck = deck;
        this.focus_cell = focus_cell;
        this.current_cell = current_cell;
        this.possible_next_cells = possible_next_cells;
    }
    render(){
        unmarkHighlightedCells()
        let is_focus_cell, is_current_cell, is_possible_next_cell
        for (let line of this.deck){
            for (let cell of line){
                is_focus_cell = false
                is_current_cell = false
                is_possible_next_cell = false
                if (cell.name == this.focus_cell){
                    is_focus_cell = true
                }
                if (cell.name == this.current_cell){
                    is_current_cell = true
                }
                if (this.possible_next_cells.includes(cell.name)){
                    is_possible_next_cell = true
                }
                cell.render(is_focus_cell, is_current_cell, is_possible_next_cell)
            }
        }
    }
    createHTMLStructure(){
        // В документе должен быть div с id = deck
        const deck = document.querySelector("#deck")
        if (deck.innerHTML != ""){
            return
        }
        let indexCell = 1
        let indexLine = 1

        for (let line of this.deck){

            let lineVisual = document.createElement("div")
            lineVisual.classList.add("line")
            indexCell++

            for (let cell of line){

                let cellVisual = document.createElement("div")
                cellVisual.classList.add("cell")
                cellVisual.setAttribute("id", cell.name)

                if ((indexCell+indexLine) % 2 === 0){
                    cellVisual.classList.add("black-cell")

                }else{
                    cellVisual.classList.add("white-cell")
                }

                indexLine++
                lineVisual.appendChild(cellVisual)
            }
            deck.appendChild(lineVisual)
        }
    }
}

class Cell {
    constructor(name, figure) {
        this.name = name;
        this.figure = figure;
    }
    getDOMElement(){ return document.querySelector(`#${this.name}`) }
    mark_with_class(valueClass){
        const DOMElement = this.getDOMElement()
        for (let elem of document.querySelectorAll("."+valueClass)){
            elem.classList.remove(valueClass)
        }
        DOMElement.classList.add(valueClass)
    }
    mark_focus_cell(){ this.mark_with_class("focus-cell") }
    mark_current_cell(){ this.mark_with_class("current-cell") }
    mark_possible_next_cell(upsideDown){ 
        if (upsideDown){
            this.getDOMElement().classList.remove("possible-next-cell") 
        }
        else{
            this.getDOMElement().classList.add("possible-next-cell") 
        }
    }

    render(is_focus_cell, is_current_cell, is_possible_next_cell){
        const DOMElement = this.getDOMElement()
        if (this.figure === null){
            DOMElement.innerHTML = ""
        }else{
            DOMElement.innerHTML = this.figure.getHTMLStructure()
        }

        if (is_focus_cell){ this.mark_focus_cell() }
        if (is_current_cell){ this.mark_current_cell() }
        if (is_possible_next_cell){ this.mark_possible_next_cell() }
        else { this.mark_possible_next_cell(true)}

    }
    removeFigure(){ this.figure = null; this.render() }
    placeFigure(figure){ this.figure = figure; this.render() }

}

const deck = new Deck()

function refreshGame(response){
    lines = []
    const deckInfo = response.deck_info
    for (let lineInfo of deckInfo.deck){
        line = []
        for (let cellInfo of lineInfo){
            line.push(
                new Cell(
                    cellInfo.name,
                    new Figure(
                        cellInfo.figure,
                        cellInfo.color
                    )
                )
            )
        }
        lines.push(line)
    }
    deck.refreshData(lines, deckInfo.focus_cell, deckInfo.current_cell, deckInfo.possible_next_cells)
    deck.createHTMLStructure()
    deck.render()
    displayMessage(response)
}

function displayMessage(response){
    document.querySelector("#message").innerHTML = response.deck_info.message.text
}

function unmarkHighlightedCells(){
    for (let typeHighlighting of ['focus-cell', 'current-cell']){
        for (let cell of document.querySelectorAll('.'+typeHighlighting)){
            cell.classList.remove(typeHighlighting)
        }
    }
}


createGameBtn.addEventListener("click", function(event){
    if (state.runningGame){
        return
    }
    customFetch(
        api.createGame.endpoint,
        api.createGame.method,
        {},
        function(response){
            state.runningGame = true
            state.gameID = response.game_id
            refreshGame(response)
        }
    )
})

document.addEventListener('keydown', function(event) {
    keys = {
        "KeyW": "w",
        "KeyA": "a",
        "KeyS": "s",
        "KeyD": "d",
        "Enter": "enter",
        "Escape": "esc",
    }
    if (event.code in keys){
        customFetch(
            api.pressKey.endpoint,
            api.pressKey.method,
            {"name": keys[event.code]},
            refreshGame
        )
    }
});